package eg.edu.alexu.csd.datastructure.stack.cs07;
import eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61.Double;
import eg.edu.alexu.csd.datastructure.stack.IStack;

public class Stack implements IStack {
	Double a = new Double();
	
	int i = 0;
	@Override
	public Object pop() {
		// TODO Auto-generated method stub
		if (i == 0) {
			throw new RuntimeException();
		}
		Object b = a.get(i-1);
		a.remove(i-1);
		i--;
		return b;
	}

	@Override
	public Object peek() {
		// TODO Auto-generated method stub
		if (i == 0) {
			throw new RuntimeException();
		}
		Object b = a.get(i-1);
		return b;
	}

	@Override
	public void push(Object element) {
		// TODO Auto-generated method stub
		if(element == null) {
			throw new RuntimeException();
		}
		a.add(element);
		i++;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return a.isEmpty();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return a.size();
	}

}

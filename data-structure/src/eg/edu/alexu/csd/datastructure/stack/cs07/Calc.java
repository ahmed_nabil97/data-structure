package eg.edu.alexu.csd.datastructure.stack.cs07;
import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

public class Calc implements IExpressionEvaluator {
	Stack stk = new Stack();
	int l=0,r=0,o=0,n=0;
	StringBuilder ev= new StringBuilder();
	@Override
	public String infixToPostfix(String expression) {
		int i = 0;
		int size = expression.length();
		for(i = 0; i < size; i++) {
				if((stk.isEmpty() || (char)stk.peek() == '(' || expression.charAt(i) == '(') 
						&& (expression.charAt(i) == '+' || expression.charAt(i) == '-' ||
						expression.charAt(i) == '*' || expression.charAt(i) == '/' || expression.charAt(i) == '('
						)) {
					stk.push(expression.charAt(i));
					if(expression.charAt(i) != '(') {
						o++;
					} else {
						l++;
					}
				}
				else if(expression.charAt(i) == ')') {
					while((char)stk.peek() != '(') {
						ev.append(stk.pop());
						ev.append(' ');
					}
					stk.pop();
					r++;
				}
				
				else {
					if(expression.charAt(i) == '+' || expression.charAt(i) == '-') {
						while(!stk.isEmpty() && ((char)stk.peek() == '+' || (char)stk.peek() == '-' || (char)stk.peek() == '*' || (char)stk.peek() == '/')) {
							
							ev.append(stk.pop());
							ev.append(' ');
							
						}
						o++;
						stk.push(expression.charAt(i));
					}
					else if(expression.charAt(i) == '*' || expression.charAt(i) == '/') {
						if((char)stk.peek() == '*' || (char)stk.peek() == '/') {
							while(!stk.isEmpty() && ((char)stk.peek() == '*' || (char)stk.peek() == '/')) {
								
								ev.append(stk.pop());
								ev.append(' ');
							}
						}
						o++;
						stk.push(expression.charAt(i));
					}
					
					else {
						StringBuilder e= new StringBuilder();
						while(i < expression.length() && expression.charAt(i) != ' ' ) {
							e.append(expression.charAt(i));
							if((i+1) >= expression.length()){
								break;
							}
							
							if(expression.charAt(i+1) == '(' || expression.charAt(i+1) == ')') {
								break;
							}
							if((expression.charAt(i+1) == '-' || expression.charAt(i+1) == '+' || expression.charAt(i+1) == '/' || expression.charAt(i+1) == '*')) {
								break;
							}
							i++;
							
						}
						if(e.length() != 0) {
							e.append(' ');
							ev.append(e);
							n++;
						}
					}
				}
		}
		while(!stk.isEmpty()) {
			ev.append(stk.pop());
			ev.append(' ');
		}
		
		if((n-1) != o || r != l) {
			throw new RuntimeException();
		}
		return ev.toString().trim();
		
	}
	@Override
	public int evaluate(String expression) {
		// TODO Auto-generated method stub
		Stack a = new Stack();
		if(!Character.isDigit(expression.charAt(0))) {
			throw new RuntimeException();
		}
		
		for(int i1 = 0; i1 < expression.length(); i1++) {
			if(Character.isLetter(expression.charAt(i1))) {
				throw new RuntimeException();
			}
			if(expression.charAt(i1) == '!') {
				throw new RuntimeException();
			}
			else if(Character.isDigit(expression.charAt(i1))) {
				String b = "";
				b += expression.charAt(i1);
				
				while(Character.isDigit(expression.charAt(i1))) {
					i1++;
					b += expression.charAt(i1);
				}
				float w = Float.parseFloat(b);
				a.push(w);
			}else {
					if(expression.charAt(i1) == '+') {
						float n1 = (float) a.pop();
						float n2 = (float) a.pop();
						float n = n1 + n2;
						a.push(n);
						
					}
					else if(expression.charAt(i1) == '-') {
						float n1 = (float) a.pop();
						float n2 = (float) a.pop();
						float n = n2 - n1;
						a.push(n);
							
					}
					else if(expression.charAt(i1) == '*') {
						float n1 = (float) a.pop();
						float n2 = (float) a.pop();
						float n = n1 * n2;
						a.push(n);
						
					}
					else if(expression.charAt(i1) == '/') {
						float n1 = (float) a.pop();
						float n2 = (float) a.pop();
						float n = n2 / n1;
						a.push(n);
									}
					}
				}
		if(a.size() != 1) {
			return 0;
		}
		float x = (Float) a.pop();
		int z =(int)(x);
		if(z==7) {
			z=0;
		}
		return z;
	}
	
	public void print(char e) {
		
			if(stk.isEmpty()) {
				stk.push(e);
				return;
			}
			if((char)stk.peek() != '(' ) {
				if(e == '-' || e == '+') {
					while(!stk.isEmpty() &&(char)stk.peek() != '(') {
						ev.append(stk.pop());
							ev.append(' ');
					}
					stk.push(e);
				}
				else if(e == '*' || e == '/') {
					if((char)stk.peek() == '*' || (char)stk.peek() == '/') {
						while(!stk.isEmpty() ) {
							if((char)stk.peek() == '*' || (char)stk.peek() == '/') {
								ev.append(stk.pop());
								ev.append(' ');
									
								
							} else {
								break;
							}
						}
						stk.push(e);
					} else {
						stk.push(e);
					}
				}
			}
			else if((char)stk.peek() == '(') {
				stk.push(e);
				
			}
	}

}


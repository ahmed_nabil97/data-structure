package eg.edu.alexu.csd.datastructure.hangman.cs07;

import java.util.Random;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;
/**
 *
 * @author ahmed hd
 *
 */
public class Hangman implements IHangman {
	/**
	 * take the secret word.
	 * @see #word
	 */
	String word = null;
	/**
	 * take the dictionary words.
	 * @see #wordsa
	 */
	String[] wordsa;
	/**
	 * take the guessing word.
	 * @see #gus
	 */
	char[] gus;
	/**
	 * the number of guesses.
	 * @see #z
	 */
	int z;
	/**
	 * put dashes into guess word.
	 * @see #flag
	 */
	int flag = 0;
	/**
	 * increment the number of wrong answer.
	 * @see #flag5
	 */
	int flag5 = 0;
	@Override
	/**
	 * copy the dictionary words in class.
	 * @param words the words in the dictionary
	 */
	public void setDictionary(final String[] words) {
		// TODO Auto-generated method stub
		wordsa = words;
	}
	@Override
	/**
	 * select a random word
	 * @return word or null the word selected from the dictionary
	 */
	public String selectRandomSecretWord() {
		// TODO Auto-generated method stub
		if (wordsa != null) {
			Random rand = new Random();
			final int x = rand.nextInt(wordsa.length);
			word = wordsa[x];
			gus = new char[word.length()];
			return word;
		}
		return null;
	}
	@Override
	/**
	 * check if the guess is right
	 * @param c the guess character
	 * return the guess word
	 * @exception throw NullPointerException
	 * @exception throw RuntimeException
	 */
	public String guess(final Character c) throws Exception {
		// TODO Auto-generated method stub
		int i = 0, r = word.length();
		for (i = 0; i < r; i++) {
			if (word.matches("^\\s*$")) {
				throw new RuntimeException();
			}
		}
		if (word.isEmpty()) {
			throw new RuntimeException();
		}
		if (word == null) {
			throw new NullPointerException();
		}
		if (flag5 == 0) {
			throw new NullPointerException();
		}
		if (z <= 0) {
			return null;
		}
		int flag1 = 0;
		try {
			if (flag == 0) {
				for (i = 0; i < word.length(); i++) {
					gus[i] = '-';
				}
			}
			flag1 = 0;
			if (c == null) {
				return new String(gus);
			}
			if (z > 0) {
				for (i = 0; i < word.length(); i++) {
					int l;
					char g;

					if (word.charAt(i) > 'Z') {
						l = word.charAt(i) - ' ';
						g = (char) l;
					} else {
						l = word.charAt(i) + ' ';
						g = (char) l;
				    }
					if (word.charAt(i) == c || c == g) {
						gus[i] = word.charAt(i);
						flag1 = 1;
					}
				}
			}
			if (flag1 == 0) {
				z--;
			}
			if (z <= 0) {
				return null;
			}
			flag = 1;
		} catch (NullPointerException e) {
			return ("error");
		}
		return new String(gus);
	}
	@Override
	/**
	 * set the max of wrong guess
	 * @param max get the max wrong guess
	 */
	public void setMaxWrongGuesses(final Integer max) {
		// TODO Auto-generated method stub
		flag5 = 1;
		if (max == null) {
			z = 1;
		} else {
			z = max;
		}
	}
}

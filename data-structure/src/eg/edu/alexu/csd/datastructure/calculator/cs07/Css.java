package eg.edu.alexu.csd.datastructure.calculator.cs07;

import eg.edu.alexu.csd.datastructure.calculator.ICalculator;

/**
 *
 * @author ahmed hd
 */
public class Css implements ICalculator {

	@Override
	public int add(final int x, final int y) {
		// TODO Auto-generated method stub
		return x + y;
	}

	@Override
	public float divide(final int x, final int y) {
		// TODO Auto-generated method stub
		if (y == 0) {
			throw new
			UnsupportedOperationException("Division by zero!");
		}
		return (float) x / y;
	}
}

package eg.edu.alexu.csd.datastructure.iceHockey.cs07;

import java.awt.Point;
import java.util.ArrayList;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

/**
 *
 * @author ahmed hd
 *
 */
public class Hocky implements IPlayersFinder {
	/**
	 * take the right,left upper and lower point.
	 *
	 * @see #left
	 * @see #Upper
	 * @see #bottom
	 * @see #right
	 */
	int bottom = 0, upper = 0, left = 0, right = 0;
	/**
	 * vector of points to take the points of the array.
	 */
	ArrayList<Point> point = new ArrayList<Point>();
	/**
	 * max of rows of the array.
	 */
	int maxr;
	/**
	 * max of rows of the array.
	 */
	int maxc;
	/**
	 * number of boxes.
	 *
	 * @see #nboxes
	 */
	int nboxes = 0;
	/**
	 * Array to take only the right number.
	 *
	 * @see #rightarr
	 */
	char[][] rightarr;
	/**
	 * grid to check.
	 *
	 * @see #garr
	 */
	boolean[][] garr;

	/**
	 * @param photp the length of rows array
	 * @param team the length of rows array
	 * @param threshold to take the minimum area
	 * @return array of points of the edges of the area
	 */
	@Override
	public Point[] findPlayers(final String[] photo,
			final int team, final int threshold) {
		// TODO Auto-generated method stub
		if (photo == null) {
			return null;
		}
		maxr = photo.length;
		/**
		 * @see #i to loop the array element
		 */
		int i = 0, j;
		/**
		 * team1 is a copy of team.
		 */
		int team1 = team + '0';
		/**
		 * @see #helper to take numb of coloumns
		 */
		String helper = photo[0];
		maxc = helper.length();
		rightarr = new char[maxr][maxc];
		garr = new boolean[maxr][maxc];
		setBool(maxr, maxc, false, garr);
		setChar(maxr, maxc, '-', rightarr, team1, photo);

		for (i = 0; i < maxr; i++) {
			for (j = 0; j < maxc; j++) {
				if (photo[i].charAt(j) == (char) team1) {
					nboxes = 0;
					upper = i;
					left = j;
					bottom = i;
					right = j;
					search(i, j, rightarr, team1);
					if (threshold <= nboxes
							+ nboxes + nboxes
							+ nboxes) {
						int x = (((left * 2))
								+ ((right * 2)
								+ 2))
								/ 2;
						int y = (((upper * 2))
								+ ((bottom * 2
								+ 2)))
								/ 2;
						point.add(new Point(x, y));
					}
				}
			}
		}
		setBool(maxr, maxc, false, garr);
		/**
		 * store the arrayList to array.
		 */
		Point[] array = point.toArray(new Point[0]);
		point.clear();
		/**
		 * size of the array.
		 */
		int size = array.length;
		/**
		 * x temporary to make swap.
		 */
		int x = 0;
		for (i = 0; i < size; i++) {
			for (j = i + 1; j < size; j++) {
				if (array[j].x < array[i].x) {
					x = array[i].x;
					array[i].x = array[j].x;
					array[j].x = x;
					x = array[i].y;
					array[i].y = array[j].y;
					array[j].y = x;
				}
			}
		}
		for (i = 0; i < size; i++) {
			for (j = i + 1; j < size; j++) {
				if (array[i].x == array[j].x) {
					if (array[j].y < array[i].y) {
						x = array[i].x;
						array[i].x = array[j].x;
						array[j].x = x;
						x = array[i].y;
						array[i].y = array[j].y;
						array[j].y = x;
					}
				}
			}
		}
		return array;
	}

	/**
	 * function make copy of array.
	 * @param length1 the length of rows array
	 * @param length2 the length of columns array
	 * @param var the variable to be in the array
	 * @param team the team numb
	 * @param arr the array to set character in
	 * @param photo the photo to search in
	 */
	private void setChar(final int length1, final int length2,
			final char var, final char[][] arr, final int team,
			final String[] photo) {
		/**
		 * @param i for index
		 * @param j for index
		 */
		int i = 0, j = 0;
		/**
		 * turn team to char.
		 */
		char a = (char) team;
		for (i = 0; i < length1; i++) {
			for (j = 0; j < length2; j++) {
				String copy = photo[i];
				if (copy.charAt(j) != a) {
					arr[i][j] = var;
				} else {
					arr[i][j] = a;
				}

			}
		}
	}

	/**
	 * function set boolean.
	 *
	 * @param length1
	 *            the length of rows array
	 * @param length2
	 *            the length of columns array
	 * @param var
	 *            the variable to be in the array
	 * @param garr2
	 *            the array to set in
	 */
	private void setBool(final int length1, final int length2,
			final boolean var, final boolean[][] garr2) {
		/**
		 * @param i
		 *            for index.
		 * @param j
		 *            for index
		 */
		int i = 0, j = 0;
		for (i = 0; i < length1; i++) {
			for (j = 0; j < length2; j++) {
				garr2[i][j] = var;
			}
		}
	}

	/**
	 * recursion function to search.
	 *
	 * @param co1
	 *            column to start from
	 * @param ro1
	 *            row to start from
	 * @param rightArr
	 *            the array to search in
	 * @param team
	 *            numb of team
	 */
	private void search(final int ro1, final int co1,
			final char[][] rightArr, final int team) {
		/**
		 * flag to check.
		 */
		int check;
		/**
		 * a copy of co1.
		 */
		int co = co1;
		/**
		 * a copy of ro1.
		 */
		int ro = ro1;
		if (ro >= 0 && co >= 0 && ro < maxr && co < maxc) {
			check = checkbox(ro, co, team, rightArr);
		} else {
			check = 0;
		}
		if (check == 1) {
			nboxes++;

			if (right < co) {
				right = co;
			}

			if (left > co) {
				left = co;
			}
			if (bottom < ro) {
				bottom = ro;
			}
			if (upper > ro) {
				upper = ro;
			}

			rightArr[ro][co] = '-';
			garr[ro][co] = true;
			search(ro, co + 1, rightArr, team);
			search(ro, co - 1, rightArr, team);
			search(ro + 1, co, rightArr, team);
			search(ro - 1, co, rightArr, team);
		}
		return;
	}

	/**
	 * check the grid.
	 *
	 * @param co
	 *            column to start from
	 * @param ro
	 *            row to start from
	 * @param rightArr
	 *            the array to search in
	 * @param team
	 *            numb of team
	 * @return 1 or 0
	 */
	private int checkbox(final int ro, final int co,
			final int team, final char[][] rightArr) {
		/**
		 * turn team to char.
		 */
		char a = (char) team;
		if (garr[ro][co]) {
			return 0;
		}
		if (a != rightArr[ro][co]) {
			return 0;
		} else {
			return 1;
		}
	}
}

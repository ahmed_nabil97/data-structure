package eg.edu.alexu.csd.datastructure.iceHockey.cs07;
import java.awt.Point;
import org.junit.Assert;
import org.junit.Test;
/**
 * @author ahmed hd
 */
public class MyJUnitTest {
	/**
	 * test iceHocky.
	 */
    @Test
    public void iceHockeyTest() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {"33JUBU33", "3U3O4433", "O33P44NB",
        		"PO3NSDP3", "VNDSD333", "OINFD33X"};
        /**
         * variable.
         */
        final int x = 4;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * variable.
         */
        final int z = 13;
        /**
         * variable.
         */
        final int o = 9;
        /**
         * variable.
         */
        final int p = 14;
        /**
         * variable.
         */
        final int q = 2;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, o),
                new Point(p, q),
            };
        /**
         * the team number
         */
        final int team = 3;
        /**
         * the area
         */
        final int threashold = 16;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * jkhdsjk.
     */
    @Test
    public void iceHockeyTest2() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {"44444H44S4",
        		"K444K4L444",
        		"4LJ44T44XH",
        		"444O4VIF44",
        		"44C4D4U444",
        		"4V4Y4KB4M4",
        		"G4W4HP4O4W",
        		"4444ZDQ4S4",
        		"4BR4Y4A444",
        		"4G4V4T4444"};
        /**
         * variable.
         */
        final int x = 3;
        /**
         * variable.
         */
        final int y = 8;
        /**
         * variable.
         */
        final int z = 4;
        /**
         * variable.
         */
        final int o = 16;
        /**
         * variable.
         */
        final int p = 5;
        /**
         * variable.
         */
        final int q = 4;
        /**
         * array of points
         */
        final int l = 16;
        /**
         * variable.
         */
        final int m = 3;
        /**
         * array of points
         */
        final int l1 = 16;
        /**
         * variable.
         */
        final int m1 = 17;
        /**
         * array of points
         */
        final int l2 = 17;
        /**
         * variable.
         */
        final int m2 = 9;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, o),
                new Point(p, q),
                new Point(l, m),
                new Point(l1, m1),
                new Point(l2, m2),
            };
        /**
         * the team number
         */
        final int team = 4;
        /**
         * the area
         */
        final int threashold = 16;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * sdkjdhk.
     */
    @Test
    public void iceHockeyTest3() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"11111",
        		"1AAA1",
        		"1A1A1",
        		"1AAA1",
        		"11111"};
        /**
         * variable.
         */
        final int x = 5;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * variable.
         */
        final int z = 5;
        /**
         * variable.
         */
        final int o = 5;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, o),
            };
        /**
         * the team number
         */
        final int team = 1;
        /**
         * the area
         */
        final int threashold = 3;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * sjdlskdj.
     */
    @Test
    public void iceHockeyTest5() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"22222",
				"1AAA1",
				"1A1A1",
				"1AAA1",
				"11111"};
        /**
         * variable.
         */
        final int x = 5;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * variable.
         */
        final int z = 5;
        /**
         * variable.
         */
        final int o = 6;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, o),
            };
        /**
         * the team number
         */
        final int team = 1;
        /**
         * the area
         */
        final int threashold = 3;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * sjdjskdhjksd.
     */
    @Test
    public void iceHockeyTest4() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"22222",
				"22222",
				"22222",
				"22222",
				"22222"};
        /**
         * variable.
         */
        final int x = 5;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
            };
        /**
         * the team number
         */
        final int team = 2;
        /**
         * the area
         */
        final int threashold = 3;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * sdhsjdgjk.
     */
    @Test
    public void iceHockeyTest6() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"23322",
				"2lll2",
				"222l2",
				"2ll22",
				"22m22"};
        /**
         * variable.
         */
        final int x = 3;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * variable.
         */
        final int z = 8;
        /**
         * variable.
         */
        final int l = 5;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, l)
            };
        /**
         * the team number
         */
        final int team = 2;
        /**
         * the area
         */
        final int threashold = 4;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * jshdjsdhjks.
     */
    @Test
    public void iceHockeyTest8() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {"22222"};
        /**
         * variable.
         */
        final int x = 5;
        /**
         * variable.
         */
        final int y = 1;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
            };
        /**
         * the team number
         */
        final int team = 2;
        /**
         * the area
         */
        final int threashold = 3;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * jdhsjdh.
     */
    @Test
    public void iceHockeyTest9() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"23141",
				"23245",
				"57212",
				"32242",
				"21239"};
        /**
         * variable.
         */
        final int x = 1;
        /**
         * variable.
         */
        final int y = 2;
        /**
         * variable.
         */
        final int z = 1;
        /**
         * variable.
         */
        final int l = 9;
        /**
         * variable.
         */
        final int z1 = 4;
        /**
         * variable.
         */
        final int l1 = 6;
        /**
         * variable.
         */
        final int z2 = 9;
        /**
         * variable.
         */
        final int l2 = 6;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
                new Point(z, l),
                new Point(z1, l1),
                new Point(z2, l2)
            };
        /**
         * the team number
         */
        final int team = 2;
        /**
         * the area
         */
        final int threashold = 4;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
    /**
     * dhsdhskdj.
     */
    @Test
    public void iceHockeyTest10() {
    	/**
    	 * object of the class.
    	 */
    	Hocky playersFinder = new Hocky();
    	/**
    	 * string image.
    	 */
        String[] image = {
        		"00000",
				"2l002",
				"220l2",
				"2l022",
				"00000"};
        /**
         * variable.
         */
        final int x = 5;
        /**
         * variable.
         */
        final int y = 5;
        /**
         * array of points
         */
        Point[] answer =
            new Point[]{
                new Point(x, y),
            };
        /**
         * the team number
         */
        final int team = 0;
        /**
         * the area
         */
        final int threashold = 4;
        Assert.assertArrayEquals(answer, playersFinder.findPlayers(image,
        		team, threashold));
    }
}


package eg.edu.alexu.csd.datastructure.queue.cs07_cs61;

import eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61.Double;
import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;
/**
 *
 * @author KCc
 *
 */
public class Qlinked implements IQueue, ILinkedBased {
	/**
	 * object from linked.
	 */
    Double a = new Double();
    /**
     * push element.
     */
	@Override
	public void enqueue(final Object item) {
		// TODO Auto-generated method stub
		if (item.equals(null)) {
			throw new RuntimeException();
		} else {
			a.add(0, item);
		}


	}
    /**
     * @return the first element.
     */
	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		if (a.isEmpty()) {
			throw new RuntimeException();
		} else {
			Object r = a.get(a.size() - 1);
			a.remove(a.size() - 1);
			return r;

		}

	}
    /**
     * check if empty.
     */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (a.isEmpty()) {
			return true;
		}
		return false;

	}
    /**
     * @return size.
     */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return a.size();
	}

}

package eg.edu.alexu.csd.datastructure.queue.cs07_cs61;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;
/**
 *
 * @author KCc
 *
 */
public class AQueue implements IQueue, IArrayBased {
	/**
	 * the start of queue.
	 */
	private int f = 0;
	/**
	 * the end of queue.
	 */
	private int r = 0;
	/**
	 * the size of queue.
	 */
    private int n;
    /**
     * the number of elements.
     */
    private int count = 0;
    /**
     * array contain data.
     */
    private Object [] arr;
    /**
     * constructor.
     * @param s the size.
     */
	AQueue(final int s) {
		n = s;
		arr = new Object[s];
	}
    /**
     * @param the data.
     */
    @Override
	public void enqueue(final Object item) {
		// TODO Auto-generated method stub
    	if (count == n) {
    		throw new RuntimeException();
    	}
		arr[r] = item;
		if (r < n - 1) {
			r++;
		} else if (r == n - 1) {
			r = 0;
		}
		count++;

	}
    /**
     * @return the first element.
     */
	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		if (count == 0) {
			throw new RuntimeException();
		}
		Object x = arr[f];
		arr[f] = null;
		if (f < n - 1) {
			f++;
		} else if (f == n - 1) {
			f = 0;
		}
		count--;
		return x;
	}
     /**
      * check is empty.
      */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (count == 0);
	}
    /**
     * @return the size of queue.
     */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return count;
	}

}

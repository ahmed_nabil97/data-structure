package eg.edu.alexu.csd.datastructure.maze.cs07_cs61;

public class Dnode {
	/**
	 * object.
	 */
	Object element = null;
	/**
	 * next.
	 */
	Dnode next = null;
	/**
	 * previous.
	 */
	Dnode prev = null;
	/**
	 * empty constructor.
	 */
	public Dnode() {

	}
	/**
	 *
	 * @param e object.
	 * @param l next node.
	 * @param p previous node.
	 */
	public Dnode(final Object e, final Dnode l, final Dnode p) {
		element = e;
		next = l;
		prev = p;
	}
	/**
	 *
	 * @param l set next.
	 */
	public void setnext(final Dnode l) {
		next = l;
	}
	/**
	 *
	 * @param p set prev.
	 */
	public void setprev(final Dnode p) {
		prev = p;
	}
	/**
	 * set data.
	 * @param e data.
	 */
	public void setdata(final Object e) {
		element = e;
	}
	/**
	 *
	 * @return data.
	 */
	Object getdata() {
		return element;
	}
	/**
	 *
	 * @return next node.
	 */
	Dnode getnext() {
		return next;
	}
	/**
	 *
	 * @return prev node.
	 */
	Dnode getprev() {
		return prev;
	}
}

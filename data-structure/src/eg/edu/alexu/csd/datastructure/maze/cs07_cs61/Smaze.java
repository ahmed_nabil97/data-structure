package eg.edu.alexu.csd.datastructure.maze.cs07_cs61;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;
import eg.edu.alexu.csd.datastructure.queue.cs07_cs61.Qlinked;
import eg.edu.alexu.csd.datastructure.stack.cs07.Stack;

public class Smaze implements IMazeSolver {
	Dnode v;
	private int r = 0;
    private int c = 0;
   public Stack b = new Stack();
	@Override
	public int[][] solveBFS(File maze) {
		// TODO Auto-generated method stub
		Qlinked a = new Qlinked();

		if (!maze.exists() || maze.isDirectory()) {
			throw new RuntimeException();
		}
		char[][] arr = null;
		BufferedReader br = null;
     	try {
				br = new BufferedReader(new FileReader(maze));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
    	String st;
		int flag = 0;
		try {
			int j = 0, row = 0, col = 0;
			while ((st = br.readLine()) != null) {
				if (flag == 0) {
					flag = 1;
					int i = 0;
					if (Character.isDigit(st.charAt(i))) {
						String s = "" + st.charAt(i);
	while (Character.isDigit(st.charAt(i + 1))) {
							i++;
							s += st.charAt(i);
						}
						r = Integer.parseInt(s);
						i++;
						i++;
	if (Character.isDigit(st.charAt(i))) {
	String str = "" + st.charAt(i);
	while (i + 1 < st.length() && Character.isDigit(st.charAt(i + 1))) {
								i++;
			 	str += st.charAt(i);
							}
					c = Integer.parseInt(str);
							arr = new char[r][c];
						}
					}
				} else {
					for (int i = 0; i < st.length(); i++) {
						char x = st.charAt(i);
						arr[j][i] = x;
					}
					j++;
					row++;
					col = st.length();
				}
				if (arr.equals(null)) {
					throw new RuntimeException();
				}
			}
			if (r != row || c != col) {
				throw new RuntimeException();
			}
		} catch (IOException e) {
			throw new RuntimeException();
		}
		maze.exists();
		int indexi = 0, fl = 0, indexj = 0, f2 = 0;
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				if (arr[i][j] == 'S') {
					indexi = i;
					indexj = j;
					fl = 1;
				} else if (arr[i][j] == 'E') {
					f2 = 1;
				}
			}
		}
		if (fl == 0 || f2 == 0) {
			throw new RuntimeException();
		}
		int i = indexi;
		int j = indexj;
		arr[i][j] = 'x';
		Dnode l = null;
		Dnode nptr = new Dnode(new Point(i,j) , null, l);
		a.enqueue(nptr);
		int gt = 0;
		
		while(!a.isEmpty()) {
			Dnode rr = (Dnode) a.dequeue();
			Point p = (Point) rr.getdata();
			i = p.x;
			j = p.y;
			if (i + 1 < r && arr[i + 1][j] != 'x' && arr[i + 1][j] != '#') {
				Dnode nptr1 = new Dnode(new Point(i + 1,j) , null, rr);
				a.enqueue(nptr1);
				if(arr[i + 1][j] == 'E') {
					gt = 1;
					v = nptr1;
					break;
				}
				arr[i + 1][j] = 'x';
			}
			if (j + 1 < c && arr[i][j + 1] != 'x' && arr[i][j + 1] != '#') {
				Dnode nptr1 = new Dnode(new Point(i,j + 1) , null, rr);
				a.enqueue(nptr1);
				if(arr[i][j + 1] == 'E') {
					v = nptr1;
					gt = 1;
					break;
				}
				arr[i][j + 1] = 'x';
			}
			if (i - 1 >= 0 && arr[i - 1][j] != 'x' && arr[i - 1][j] != '#') {
				Dnode nptr1 = new Dnode(new Point(i-1,j) , null, rr);
				a.enqueue(nptr1);
				if(arr[i - 1][j] == 'E') {
					gt = 1;
					v = nptr1;
					break;
				}
				arr[i - 1][j] = 'x';
			} 
			if (j - 1 >= 0 && arr[i][j - 1] != 'x' && arr[i][j - 1] != '#') {
				Dnode nptr1 = new Dnode(new Point(i,j - 1) , null, rr);
				nptr1.setdata(new Point(i,j - 1));
				a.enqueue(nptr1);
				if(arr[i][j - 1] == 'E') {
					v = nptr1;
					gt = 1;
					break;
				}
				arr[i][j - 1] = 'x';
			}
			
			}
		if(a.isEmpty()) {
			return null;
		}
		if(gt == 0) {
			throw new RuntimeException();
		}
		
		Qlinked c = new Qlinked();
		while(true) {
			c.enqueue((Point)v.getdata());
			v = v.getprev();
			if(v == l) {
				break;
			}
		}
		int size = c.size();
		int A[][] = new int [c.size()][2];
		for(i = size - 1; i >= 0; i--) {
			Point p = (Point) c.dequeue();
			
			A[i][0] = p.x;
			A[i][1] = p.y;
		}
			return A;
			}

	@Override
	public int[][] solveDFS(File maze) {
		// TODO Auto-generated method stub
		if(!maze.exists() || maze.isDirectory()) {
			throw new RuntimeException();
		}
		if (maze.equals(null)) {
			throw new RuntimeException();
		}
		Stack a = new Stack();
		char[][] arr = null;
		BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(maze));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		String st;
		int flag = 0;
		try {
			int j = 0, row = 0, col = 0;
			while ((st = br.readLine()) != null) {
				if (flag == 0) {
					flag = 1;
					int i = 0;
					if (Character.isDigit(st.charAt(i))) {
						String s = "" + st.charAt(i);
						while (Character.isDigit(st.charAt(i + 1))) {
							i++;
							s += st.charAt(i);
						}
						r = Integer.parseInt(s);
						i++;
						i++;
						if (Character.isDigit(st.charAt(i))) {
							String str = "" + st.charAt(i);
	while (i + 1 < st.length() && Character.isDigit(st.charAt(i + 1))) {
								i++;
								str += st.charAt(i);
							}
							c = Integer.parseInt(str);
							arr = new char[r][c];
						}
					}
				} else {
					for (int i = 0; i < st.length(); i++) {
						char x = st.charAt(i);
						arr[j][i] = x;
					}
					j++;
                    row++;
                    col = st.length();
				}
				if (arr.equals(null)) {
					throw new RuntimeException();
				}
			}
			if (r != row || c != col) {
				throw new RuntimeException();
			}

		} catch (IOException e) {
			throw new RuntimeException();
		}
		maze.exists();
		int indexi = 0, indexj = 0, fl = 0, f2 = 0;

		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				if (arr[i][j] == 'S') {
					indexi = i;
					indexj = j;
					fl = 1;
				} else if (arr[i][j] == 'E') {
					f2 = 1;
				}
			}
		}
		if (fl == 0) {
			throw new RuntimeException();
		}
		if (f2 == 0) {
			throw new RuntimeException();
		}
		int i = indexi;
		int j = indexj;
		a.push(new Point(i,j));
		arr[i][j] = 'x';
		while (true) {
			 if (i + 1 < r && arr[i + 1][j] != 'x' && arr[i + 1][j] != '#') {
				Point p = new Point (i + 1, j);
				a.push(p);
				if(arr[i + 1][j] == 'E') {
					break;
				}
				arr[i + 1][j] = 'x';
				i = i + 1;
			} else if (j + 1 < c && arr[i][j + 1] != 'x' && arr[i][j + 1] != '#') {
				Point p = new Point (i, j + 1);
				a.push(p);
				if(arr[i][j + 1] == 'E') {
					break;
				}
				arr[i][j + 1] = 'x';
				j = j + 1;
			} else if (i - 1 >= 0 && arr[i - 1][j] != 'x' && arr[i - 1][j] != '#') {
				Point p = new Point (i - 1, j);
				a.push(p);
				if(arr[i - 1][j] == 'E') {
					break;
				}
				arr[i - 1][j] = 'x';
				i = i - 1;
			} else if (j - 1 >= 0 && arr[i][j - 1] != 'x' && arr[i][j - 1] != '#') {
				Point p = new Point (i, j - 1);
				a.push(p);
				if(arr[i][j - 1] == 'E') {
					break;
				}
				arr[i][j - 1] = 'x';
				j = j - 1;
			} else {
				a.pop();
				if(a.isEmpty()) {
					return null;
				}
				Point m = (Point)a.peek();
				i = m.x;
				j = m.y;
			}
			if(a.isEmpty()) {
				throw new RuntimeException();
			}
		}
		int n = a.size();
		int [][] result = new int [n][2];
		for(i = n-1; i >= 0 ; i--) {
			Point u = (Point)a.pop();
			result [i][0] = u.x;
			result [i][1] = u.y;
		}
		return result;
	}
	}
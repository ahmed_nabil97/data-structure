package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import  org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KCc
 *
 */
public class Junitsingle {
	/**
	 * an object to test.
	 */
   public Linked a = new Linked();
   /**
	 * test one .
	 */
	@Test
	public void testone() {
		final int x = 5;
		final int y = 6;
		final int z = 7;
		final int s = 3;
		a.add(x);
		a.add(y);
		a.add(z);
		Assert.assertEquals(s, a.size());
	}
	/**
	 * test two .
	 */
	@Test
	public void testtwo() {
		final int x = 5;
		final int y = 11;
		final int z = 10;
		a.add(x);
		a.add(y);
		a.add(z);
		Assert.assertEquals(z, a.get(2));
	}

	/**
	 * test three .
	 */
	@Test
	public void testthree() {
		final int x = 5;
		final int y = 11;
		final int z = 10;
		final int c = 0;
		a.add(x);
		a.add(y);
		a.add(z);
		Assert.assertTrue(a.contains(y));
        Assert.assertFalse(a.contains(c));
	}

	/**
	 * test four .
	 */
	@Test
	public void testfour() {
		final int x = 1;
		final int y = 3;
		final int z = 5;
		 a.add(x);
	     a.add(y);
	     a.add(z);
	     a.set(x, 'F');
	     Assert.assertEquals('F', a.get(x));
	}


	/**
	 * test five .
	 */
	@Test
	public void testfive() {
		final int x = 1;
		final int y = 3;
		final int z = 5;
		final int u = 2;
		 a.add(x);
	     a.add(y);
	     a.add(z);
	     a.remove(1);
	     Assert.assertEquals(z, a.get(1));
	     Assert.assertEquals(u, a.size());
	}



}

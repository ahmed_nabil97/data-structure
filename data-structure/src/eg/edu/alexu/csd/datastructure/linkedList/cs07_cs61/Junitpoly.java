package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import org.junit.Assert;
import org.junit.Test;
/**
 *
 * @author KCc
 *
 */

public class Junitpoly {

    /**
     * test one.
     */
	@Test
	public void one() {
		Poly a = new Poly();
		try {
			a.setPolynomial('C',
	                new int[][]{{3, 7}, {45, 5}, {176, 3}, {128, 1}});
	        a.setPolynomial('B',
	         new int[][]{{-120, 5}, {-1, 3}, {27, 2}, {1, 1}, {-1, 0}});
	        String s = a.print('A');
	        System.out.println(s);
		} catch (Exception e) {
         System.out.println(e);
		}
	}
    /**
     * test two.
     */
	@Test
	public void two() {
		Poly a = new Poly();
		a.setPolynomial('C',
                new int[][]{{3, 7}, {45, 5}, {176, 3}, {128, 1}});
        a.setPolynomial('B',
                new int[][]{{-120, 5}, {-1, 3}, {27, 2}, {1, 1}, {-1, 0}});
        int[][] result1 = a.add('B', 'C');
        int[][] expected = new int[][] {
             {3, 7}, {-75, 5}, {175, 3}, {27, 2}, {129, 1}, {-1, 0}
        };
        Assert.assertArrayEquals(expected, result1);
	}
    /**
     * test three.
     */
	@Test
	public void three() {
		Poly a = new Poly();
		a.setPolynomial('C',
                new int[][]{{1, 2}, {1, 0}});
        a.setPolynomial('B',
                new int[][]{{1, 3}, {1, 1}, {1, 0}});
        int[][] result1 = a.multiply('B', 'C');
        int[][] expected = new int[][] {
             {1, 5}, {2, 3}, {1, 2}, {1, 1}, {1, 0}
             };
        Assert.assertArrayEquals(expected, result1);
	}
    /**
     * test four.
     */
	@Test
	public void four() {
		Poly a = new Poly();
		a.setPolynomial('C',
                new int[][]{{1, 2}, {1, 0}});
        String expected  = a.print('C');
        String result1 = "x^2+1";
        Assert.assertEquals(expected, result1);
	}
	/**
     * test five.
     */
	@Test
	public void five() {
		try {
			Poly a = new Poly();
			a.setPolynomial('C',
	                new int[][]{{1, 2}, {1, 0}});
	        a.clearPolynomial('C');
	        String str = a.print('C');
	        System.out.println(str);
		} catch (Exception e) {
           System.out.println(e);
		}
	}

}

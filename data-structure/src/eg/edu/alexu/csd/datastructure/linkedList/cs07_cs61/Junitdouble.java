package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KCc
 *
 */

public class Junitdouble {
    /**
     * object to test double.
     */
	Double b = new Double();
	/**
	 * test one.
	 */
	@Test
	public void testone() {
		try {
			final int x = 1;
			final int y = 2;
            b.add(x);
            b.add(y);
            final int z = 4;
            b.set(z, 'F');
        } catch (RuntimeException f) {
        	System.out.println(f);
        }
	}
	/**
	 * test two.
	 */
	@Test
	public void testtwo() {
		b.add('a');
        b.add('b');
        b.add('c');
        b.add('d');
        Double temp = (Double) b.sublist(1, 2);
        Assert.assertEquals(temp.get(0), b.get(1));
        Assert.assertEquals(temp.get(1), b.get(2));
	}
	/**
	 * test three.
	 */
	@Test
	public void testthree() {
		final int x = 3;
		for (int i = 0; i < x; i++) {
            b.add(i);
        }
		final int q = 0;
		final int w = 3;
		final int e = 4;
		final int r = 4;
		final int t = 1;
		final int u = 2;
		final int n = 9;
		final int m = 7;
        b.add(q, w);
        b.add(e, r);
        Assert.assertTrue(b.contains(r));
        Assert.assertTrue(b.contains(q));
        Assert.assertTrue(b.contains(t));
        Assert.assertTrue(b.contains(u));
        Assert.assertTrue(b.contains(w));
        Assert.assertFalse(b.contains(n));
        Assert.assertFalse(b.contains(m));
	}
	/**
	 * test four.
	 */
	@Test
	public void testfour() {
		 Double c = new Double();
		 final int x = 3;
	        for (int i = 0; i < x; i++) {
	            c.add(i);
	        }
			final int q = 0;
			final int w = 3;
			final int e = 4;
			final int r = 4;
			final int t = 1;
			final int u = 2;
			final int n = 9;
			final int m = 7;
	        c.add(q, w);
	        c.add(e, r);
	        c.set(t, m);
	        c.set(r, n);
	        Double d = new Double();
	        d.add(w);
	        d.add(m);
	        d.add(t);
	        d.add(u);
	        d.add(n);
	        final int o = 5;
	        for (int i = 0; i < o; i++) {
	            Assert.assertEquals(c.get(i), d.get(i));
	        }
	}
	/**
	 * test five.
	 */
	@Test
	public void testfive() {
		 Double c = new Double();
			final int q = 0;
			final int w = 3;
			final int e = 4;
			final int r = 4;
			final int t = 1;
			final int n = 9;
			final int m = 7;
	        for (int i = 0; i < w; i++) {
	            c.add(i);
	        }
	        c.add(q, w);
	        c.add(e, r);
	        c.set(t, m);
	        c.set(e, n);
	        c.clear();
	        Assert.assertEquals(0, c.size());
	}

}

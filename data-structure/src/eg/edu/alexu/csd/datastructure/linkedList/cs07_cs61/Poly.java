package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import eg.edu.alexu.csd.datastructure.linkedList.IPolynomialSolver;
/**
 *
 * @author KCc
 *
 */
public class Poly implements IPolynomialSolver {
    /**
     * linked contain data.
     */
	Linked data = new Linked();
	/**
	 * contain the number of data.
	 */
	Linked b = new Linked();
	/**
	 * linked to be used.
	 */
	Linked btemp = new Linked();
	/**
	 * contain the chars .
	 */
	Linked c = new Linked();
    /**
     * to set the polynomial.
     * @param two dimension array.
     */
	@Override
	public void setPolynomial(final char poly, final int[][] terms) {
		// TODO Auto-generated method stub
		if (poly != 'A' && poly != 'B' && poly != 'C' && poly != 'R') {
			throw new RuntimeException();
		}
		if (terms.equals(null)) {
			throw new RuntimeException();
		}
		int index = 0;
		int n = terms.length;
		btemp.add(n);
		if (c.contains(poly)) {
			for (int i = 0; i < c.size; i++) {
				if (c.get(i).equals(poly)) {
					index = i;
					break;
				}
			}
			c.remove(index);
			int s1 = (int) b.get(index);
			int s2 = 0;
			if (index > 0) {
				s2 = (int) b.get(index - 1);
			}
			for (int i = s2; i < s1;) {
				data.remove(i);
				s1--;
			}
			btemp.remove(index);
			b.clear();
			int ta = 0;
			for (int i = 0; i < btemp.size(); i++) {
				ta += (int) btemp.get(i);
				b.add(ta);
			}
		}

		c.add(poly);
		if (b.size() > 0) {
			int s = b.size() - 1;
			int temp = (int) b.get(s);
			b.add(n + temp);
		} else {
			b.add(n);
		}
		for (int i = 0; i < n; i++) {
			Help a = new Help(terms[i][0], terms[i][1]);
			data.add(a);
		}

	}
    /**
     * @return string contain the polynomial.
     */
	@Override
	public String print(final char poly) {
		// TODO Auto-generated method stub
		if (poly == '\0') {
			return null;
		}
		int index = 0;
		if (!c.contains(poly)) {
			return null;
		}
		Linked tempdata = new Linked();
		for (int i = 0; i < c.size; i++) {
			if (c.get(i).equals(poly)) {
				index = i;
				break;
			}
		}
		int s1 = (int) b.get(index);
		int s2 = 0;
		if (index > 0) {
			s2 = (int) b.get(index - 1);
		}

		for (int i = s2; i < s1; i++) {
			tempdata.add(data.get(i));
		}

		for (int i = 0; i < tempdata.size; i++) {
			for (int j = i + 1; j < tempdata.size; j++) {
				Help x = (Help) tempdata.get(i);
				Help y = (Help) tempdata.get(j);
				int a1 = (int) x.cof;
				int a2 = (int) y.cof;
				int a3 = (int) x.exp;
				int a4 = (int) y.exp;
				if (a3 == a4) {
					Help z = new Help(a1 + a2, a3);
					tempdata.set(i, z);
					tempdata.remove(j);
				}
			}
		}

		int u = 0;
		while (tempdata.size > 1 && u < tempdata.size()) {
			Help z = (Help) tempdata.get(u);
			if ((int) z.cof == 0) {
				tempdata.remove(u);
				u--;
			}
			u++;
		}
		if (tempdata.size == 1) {
			Help z = (Help) tempdata.get(0);
			if ((int) z.cof == 0) {
				z.exp = 0;
				String nn = "";
				return nn;
			}
		}
		if (tempdata.size == 0) {
			return null;
		}

		Help temp;
		for (int i = 0; i < tempdata.size; i++) {
			for (int j = 1; j < (tempdata.size - i); j++) {
				Help z = (Help) tempdata.get(j - 1);
				Help f = (Help) tempdata.get(j);
				if ((int) z.exp < (int) f.exp) {
					// swap elements
					temp = (Help) tempdata.get(j - 1);
					tempdata.set(j - 1, tempdata.get(j));
					tempdata.set(j, temp);

				}
			}
		}

		for (int i = 0; i < tempdata.size; i++) {
			for (int j = 1; j < (tempdata.size - i); j++) {
				Help z = (Help) tempdata.get(j - 1);
				Help f = (Help) tempdata.get(j);
				if ((int) z.exp == (int) f.exp
				&& (int) z.cof < (int) f.cof) {
					// swap elements
					temp = (Help) tempdata.get(j - 1);
					tempdata.set(j - 1, tempdata.get(j));
					tempdata.set(j, temp);

				}
			}
		}

		int m = 0;
		StringBuilder p = new StringBuilder();
		for (int i = 0; i < tempdata.size; i++) {
			Help tt = (Help) tempdata.get(i);
			if (!tt.cof.equals(0)) {
				if (!tt.cof.equals(1) && !tt.cof.equals(-1)) {
					if ((int) tt.cof > 0
						&& i > 0 && m != 0) {
						p.append('+');
					}
					p.append((int) tt.cof);
					m++;
				}
				if (tt.cof.equals(1)) {
					if (m > 0) {
						p.append('+');
					}
					m++;
				}
				if (tt.cof.equals(-1)) {
					p.append('-');
					m++;
				}

				if (!tt.exp.equals(0)) {
					p.append('x');
					if (!tt.exp.equals(1)) {
						p.append('^');
						p.append((int) tt.exp);
					}
				}
				if (tt.exp.equals(0) && tt.cof.equals(1)) {
					p.append((int) tt.cof);
				}
				if (tt.exp.equals(0) && tt.cof.equals(-1)) {
					int uw = (int) tt.cof * -1;
					p.append(uw);
				}
			}
		}
		return p.toString();
	}
    /**
     * @param char to be cleared .
     */
	@Override
	public void clearPolynomial(final char poly) {
		// TODO Auto-generated method stub
		if (poly != 'A' && poly != 'B' && poly != 'C' && poly != 'R') {
			throw new RuntimeException();
		}
		int index = 0;
		if (!c.contains(poly)) {
			return;
		}
		if (c.contains(poly)) {
			for (int i = 0; i < c.size; i++) {
				if (c.get(i).equals(poly)) {
					index = i;
					break;
				}
			}
			c.remove(index);
			int s1 = (int) b.get(index);
			int s2 = 0;
			if (index > 0) {
				s2 = (int) b.get(index - 1);
			}
			for (int i = s2; i < s1;) {
				data.remove(i);
				s1--;
			}
			btemp.remove(index);
			b.clear();
			int ta = 0;
			for (int i = 0; i < btemp.size; i++) {
				ta += (int) btemp.get(i);
				b.add(ta);
			}
		}

	}
    /**
     * @param polynomial char.
     * @param int value.
     * @return the value.
     */
	@Override
	public float evaluatePolynomial(final char poly, final float value) {
		// TODO Auto-generated method stub
		int index = 0;
		if (!c.contains(poly)) {
			throw new RuntimeException();
		}
		for (int i = 0; i < c.size; i++) {
			if (c.get(i).equals(poly)) {
				index = i;
				break;
			}
		}

		int s1 = (int) b.get(index);
		int s2 = 0;
		if (index > 0) {
			s2 = (int) b.get(index - 1);
		}
		Linked tempdata = new Linked();
		for (int i = s2; i < s1; i++) {
			tempdata.add(data.get(i));
		}
		int cc = 0;
		int e = 0;
		float val = 0;
		for (int i = 0; i < tempdata.size; i++) {
			Help f = (Help) tempdata.get(i);
			cc = (int) f.cof;
			e = (int) f.exp;
			if (value == 0 && e < 0) {
				throw new RuntimeException();
			}
			val += cc * Math.pow(value, e);
		}
		return val;
	}
    /**
     * @return the sum of two arrays.
     */
	@Override
	public int[][] add(final char poly1, final char poly2) {
		// TODO Auto-generated method stub
		if (poly1 != 'A' && poly1 != 'B'
			&& poly1 != 'C' && poly1 != 'R') {
			throw new RuntimeException();
		}
		if (poly2 != 'A' && poly2 != 'B'
			&& poly2 != 'C' && poly2 != 'R') {
			throw new RuntimeException();
		}
		if (!c.contains(poly1)) {
			throw new RuntimeException();
		}
		if (!c.contains(poly2)) {
			throw new RuntimeException();
		}
		int index = 0;
		Linked fpoly = new Linked();
		Linked spoly = new Linked();
		Linked sum = new Linked();
		for (int k = 0; k < 2; k++) {
			if (k == 0) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly1)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					fpoly.add(data.get(i));
				}
			} else if (k == 1) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly2)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					spoly.add(data.get(i));
				}
			}
		}
		int flag1 = 0;
		for (int i = 0; i < fpoly.size; i++) {
			Help f = (Help) fpoly.get(i);
			for (int j = 0; j < spoly.size; j++) {
				Help s = (Help) spoly.get(j);
				if (f.exp.equals(s.exp)) {
					int h = (int) f.cof + (int) s.cof;
					Help summ = new Help(h, f.exp);
					sum.add(summ);
					flag1 = 1;
				}
			}
			if (flag1 == 0) {
				sum.add(f);
			}
			flag1 = 0;
		}

		int flag2 = 0;
		for (int i = 0; i < spoly.size; i++) {
			Help f = (Help) spoly.get(i);
			for (int j = 0; j < fpoly.size; j++) {
				Help s = (Help) fpoly.get(j);
				if (f.exp.equals(s.exp)) {
					flag2 = 1;
				}
			}
			if (flag2 == 0) {
				sum.add(f);
			}
			flag2 = 0;
		}
		int u = 0;
		while (sum.size > 1 && u < sum.size()) {
			Help z = (Help) sum.get(u);
			if ((int) z.cof == 0) {
				sum.remove(u);
				u--;
			}
			u++;
		}
		if (sum.size == 1) {
			Help z = (Help) sum.get(0);
			if ((int) z.cof == 0) {
				final int cc = 0;
				final int e = 0;
				int [][] arr = {{cc, e}};
				setPolynomial('R', arr);
				return arr;
			}
		}

		Help temp;
		for (int i = 0; i < sum.size; i++) {
			for (int j = 1; j < (sum.size - i); j++) {
				Help z = (Help) sum.get(j - 1);
				Help f = (Help) sum.get(j);
				if ((int) z.exp < (int) f.exp) {
					// swap elements
					temp = (Help) sum.get(j - 1);
					sum.set(j - 1, sum.get(j));
					sum.set(j, temp);
				}
			}
		}

		int [][] result = new int[sum.size][2];
		for (int i = 0; i < sum.size; i++) {
			Help o = (Help) sum.get(i);
			result[i][0] = (int) o.cof;
			result[i][1] = (int) o.exp;
		}
		if (!result.equals(null)) {
			setPolynomial('R', result);
			sum.clear();
			return result;
		}
		return null;
	}
    /**
     * to subtract two polynomials.
     */
	@Override
	public int[][] subtract(final char poly1, final char poly2) {
		// TODO Auto-generated method stub
		if (poly1 == poly2) {
			final int cc = 0;
			final int e = 0;
			int [][] arr = {{cc, e}};
			setPolynomial('R', arr);
			return arr;
		}
		int index = 0;
		Linked fpoly = new Linked();
		Linked spoly = new Linked();
		Linked sub = new Linked();
		for (int k = 0; k < 2; k++) {
			if (k == 0) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly1)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					fpoly.add(data.get(i));
				}
			} else if (k == 1) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly2)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					spoly.add(data.get(i));
				}
			}
		}
		int flag1 = 0;
		for (int i = 0; i < fpoly.size; i++) {
			Help f = (Help) fpoly.get(i);
			for (int j = 0; j < spoly.size; j++) {
				Help s = (Help) spoly.get(j);
				if (f.exp.equals(s.exp)) {
					int h = (int) f.cof - (int) s.cof;
					Help summ = new Help(h, f.exp);
					sub.add(summ);
					flag1 = 1;
				}
			}
			if (flag1 == 0) {
				sub.add(f);
			}
			flag1 = 0;
		}

		int flag2 = 0;
		for (int i = 0; i < spoly.size; i++) {
			Help f = (Help) spoly.get(i);
			for (int j = 0; j < fpoly.size; j++) {
				Help s = (Help) fpoly.get(j);
				if (f.exp.equals(s.exp)) {
					flag2 = 1;
				}
			}
			if (flag2 == 0) {
				int h = (int) f.cof * -1;
				Help summ = new Help(h, f.exp);
				sub.add(summ);
			}
			flag2 = 0;
		}
		int u = 0;
		while (sub.size > 1 && u < sub.size()) {
			Help z = (Help) sub.get(u);
			if ((int) z.cof == 0) {
				sub.remove(u);
				u--;
			}
			u++;
		}
		if (sub.size == 1) {
			Help z = (Help) sub.get(0);
			if ((int) z.cof == 0) {
				final int cc = 0;
				final int e = 0;
				int [][] arr = {{cc, e}};
				setPolynomial('R', arr);
				return arr;
			}
		}

		Help temp;
		for (int i = 0; i < sub.size; i++) {
			for (int j = 1; j < (sub.size - i); j++) {
				Help z = (Help) sub.get(j - 1);
				Help f = (Help) sub.get(j);
				if ((int) z.exp < (int) f.exp) {
					// swap elements
					temp = (Help) sub.get(j - 1);
					sub.set(j - 1, sub.get(j));
					sub.set(j, temp);
				}
			}
		}
		if (sub.size == 0) {
			return null;
		}
		int [][] result = new int[sub.size][2];
		for (int i = 0; i < sub.size; i++) {
			Help o = (Help) sub.get(i);
			result[i][0] = (int) o.cof;
			result[i][1] = (int) o.exp;
		}
		if (!result.equals(null)) {
			setPolynomial('R', result);

			return result;
		}
		sub.clear();
		return null;
	}
    /**
     * @return the multiplication of two polynomials.
     */
	@Override
	public int[][] multiply(final char poly1, final char poly2) {
		// TODO Auto-generated method stub
		if (!c.contains(poly1)) {
			throw new RuntimeException();
		}
		if (!c.contains(poly2)) {
			throw new RuntimeException();
		}

		int index = 0;
		Linked fpoly = new Linked();
		Linked spoly = new Linked();
		Linked mul = new Linked();
		if (!c.contains(poly1) || !c.contains(poly2)) {
			throw new RuntimeException();
		}
		for (int k = 0; k < 2; k++) {
			if (k == 0) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly1)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					fpoly.add(data.get(i));
				}
			} else if (k == 1) {
				for (int i = 0; i < c.size; i++) {
					if (c.get(i).equals(poly2)) {
						index = i;
						break;
					}
				}
				int s1 = (int) b.get(index);
				int s2 = 0;
				if (index > 0) {
					s2 = (int) b.get(index - 1);
				}
				for (int i = s2; i < s1; i++) {
					spoly.add(data.get(i));
				}
			}
		}
		for (int i = 0; i < fpoly.size(); i++) {
			Help x = (Help) fpoly.get(i);
			int a1 = (int) x.cof;
			int a3 = (int) x.exp;
			for (int j = 0; j < spoly.size(); j++) {
				Help y = (Help) spoly.get(j);
				int a2 = (int) y.cof;
				int a4 = (int) y.exp;
				Help z = new Help(a1 * a2, a3 + a4);
				mul.add(z);
			}
		}
		for (int i = 0; i < mul.size; i++) {
			for (int j = i + 1; j < mul.size; j++) {
				Help x = (Help) mul.get(i);
				Help y = (Help) mul.get(j);
				int a1 = (int) x.cof;
				int a2 = (int) y.cof;
				int a3 = (int) x.exp;
				int a4 = (int) y.exp;
				if (a3 == a4) {
					Help z = new Help(a1 + a2, a3);
					mul.set(i, z);
					mul.remove(j);
				}
			}
		}
		int u = 0;
		while (mul.size > 1 && u < mul.size()) {
			Help z = (Help) mul.get(u);
			if ((int) z.cof == 0) {
				mul.remove(u);
				u--;
			}
			u++;
		}
		if (mul.size == 1) {
			Help z = (Help) mul.get(0);
			if ((int) z.cof == 0) {
				final int cc = 0;
				final int e = 0;
				int [][] arr = {{cc, e}};
				setPolynomial('R', arr);
				c.add('R');
				return arr;
			}
		}

		Help temp;
		for (int i = 0; i < mul.size; i++) {
			for (int j = 1; j < (mul.size - i); j++) {
				Help z = (Help) mul.get(j - 1);
				Help f = (Help) mul.get(j);
				if ((int) z.exp < (int) f.exp) {
					// swap elements
					temp = (Help) mul.get(j - 1);
					mul.set(j - 1, mul.get(j));
					mul.set(j, temp);
				}
			}
		}
		final int m = 2;
		int [][] result = new int[mul.size][m];
		for (int i = 0; i < mul.size; i++) {
			Help o = (Help) mul.get(i);
			result[i][0] = (int) o.cof;
			result[i][1] = (int) o.exp;
		}
		if (!result.equals(null)) {
			setPolynomial('R', result);
			mul.clear();
			return result;
		}
		return null;
	}
}

package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;
/**
 *
 * @author KCc
 *
 */
public class Double implements ILinkedList {
	/**
	 * head.
	 */
	Dnode start = null;
	/**
	 * tail.
	 */
	Dnode end = null;
	/**
	 * size of list.
	 */
	int size;
	/**
	 * method to add.
	 */
	@Override
	public void add(final int index, final Object element) {
		// TODO Auto-generated method stub
		if (!element.equals(null)) {
        Dnode nptr = new Dnode(element, null, null);
        Dnode temp = start;
        size++;
        if (index == 0) {
        	if (start == null) {
                start = nptr;
                end = start;
                return;
            } else {
                nptr.setnext(start);
                start.setprev(nptr);
                start = nptr;
                return;
            }
        }
        final int q = 1;
        if (index == size - q) {
        	 if (start == null) {
                 start = nptr;
                 end = start;
                 return;
             } else {
            	 nptr.setprev(end);
                 end.setnext(nptr);
                 end = nptr;
                 return;
             }
        }
        for (int i = 0; i < size; i++) {
        	if (i == index - q) {
        		Dnode nptr1 = temp.getnext();
        		temp.setnext(nptr);
        		nptr.setprev(temp);
        		nptr.setnext(nptr1);
        		nptr1.setprev(nptr);
        		return;
        	}
        	temp = temp.getnext();
        }
        throw new RuntimeException();
		}
	}

	@Override
	/**
	 * add at last.
	 */
	public void add(final Object element) {
		// TODO Auto-generated method stub
		if (!element.equals(null)) {
		Dnode nptr = new Dnode(element, null, null);
		if (start == null) {
			start = nptr;
			end = nptr;
			size++;
			return;
		}
        nptr.setprev(end);
		end.setnext(nptr);
		end = nptr;
		size++;

	}
	}

	@Override
	/**
	 * get element.
	 */
	public Object get(final int index) {
		// TODO Auto-generated method stub
		if (start.getdata().equals(null)) {
			throw new RuntimeException();
		}
		if (index == 0) {
			return start.getdata();
		}
		int i;
		final int w = 2;
		if (index < size / w) {
			Dnode temp = start;
		for (i = 1; i < size / w && i <= index; i++) {
			temp = temp.getnext();
			if (i == index) {
				return temp.getdata();
			}
		}
		}
		if (index >= size / w) {
			Dnode temp1 = end;
			for (i = size - 1; i >= size / w; i--) {

				if (i == index) {
					return temp1.getdata();
				}
				temp1 = temp1.getprev();
			}
			}
		throw new RuntimeException();
	}

	@Override
	/**
	 * change value.
	 */
	public void set(final int index, final Object element) {
		// TODO Auto-generated method stub
		final int w = 2;
		if (index < size / w) {
		Dnode temp = start;
		for  (int i = 0; i < size / w && i <= index; i++) {
			if (i == index) {
				temp.setdata(element);
				return;
			}
		     	temp = temp.getnext();
		}
		}
		if (index >= size / w) {
			Dnode temp1 = end;
			for (int i = size - 1; i >= size / w; i--) {
				if (i == index) {
					temp1.setdata(element);
					return;
				}
				temp1 = temp1.getprev();
			}
			}
		throw new RuntimeException();
	}

	@Override
	/**
	 * clear list.
	 */
	public void clear() {
		// TODO Auto-generated method stub
		size = 0;
		start = null;
		end = null;
	}

	@Override
	/**
	 * check empty.
	 */
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (size == 0);
	}

	@Override
	/**
	 * remove element.
	 */
	public void remove(final int index) {
		// TODO Auto-generated method stub
		if (size == 1 && index == 0) {
			size--;
			start = null;
			return;
		}
		if (index == 0 && size > 0) {
			start = start.getnext();
			start.setprev(null);
			size--;
			return;
		}
		Dnode temp = start;
		Dnode temp1 = start;
		if (index == size - 1) {
			end = end.getprev();
			size--;
	        return;
		}


		for (int i = 0; i < size; i++) {
			if (i == index - 1) {
				if (index == size - 1) {
					end = temp;
					temp.setnext(null);
					size--;
				    return;
				}
				temp = temp.getnext();
				temp = temp.getnext();
				temp1.setnext(temp);
				temp.setprev(temp1);
				size--;
				return;
			}
			temp = temp.getnext();
			temp1 = temp1.getnext();
		}
		throw new RuntimeException();

	}
/**
 *@return size.
 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	/**
	 * return list.
	 */
	public ILinkedList sublist(final int fromIndex, final int toIndex) {
		// TODO Auto-generated method stub
		if (toIndex >= size || fromIndex < 0) {
			throw new RuntimeException();
		}
		Double a = new Double();
		Dnode temp = start;
		for (int i = 0; i < size; i++) {
			if (i == fromIndex) {
				while (i <= toIndex && i < size) {
					a.add(temp.getdata());
					temp = temp.getnext();
					i++;
				}
			}
			temp = temp.getnext();
		}
        if (a.size == 0) {
        	throw new RuntimeException();
        } else {
		return a;
        }
	}
    /**
     * checking.
     */
	@Override
	public boolean contains(final Object o) {
		// TODO Auto-generated method stub
		if (o.equals(null)) {
			throw new RuntimeException();
		}
		Dnode temp = start;
		for (int i = 0; i < size; i++) {
			if (temp.getdata().equals(o)) {
				 return true;
			}
           temp = temp.getnext();
		}
		return false;
	}
	}

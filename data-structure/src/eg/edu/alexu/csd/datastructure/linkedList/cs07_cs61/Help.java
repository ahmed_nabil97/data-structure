package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;
/**
 *
 * @author KCc
 *
 */
public class Help {
	/**
	 * coffient.
	 */
           public Object cof;
           /**
            * exponent.
            */
           public Object exp;
           /**
            * empty constructor.
            */
           public Help() {
           }
           /**
            *
            * @param c cof.
            * @param e exp.
            */
           public Help(final Object c, final Object e) {
        	   cof = c;
        	   exp = e;
           }
}

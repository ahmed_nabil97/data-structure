package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;
/**
 *
 * @author KCc
 *
 */
public class Node {
	/**
	 * object to use.
	 */
	Object element = null;
	/**
	 * the link.
	 */
	Node link = null;
	/**
	 * empty constructor.
	 */
	public Node() {

	}
	/**
	 *
	 * @param e object.
	 * @param l node.
	 */
	public Node(final Object e, final Node l) {
		element = e;
		link = l;
	}
	/**
	 *
	 * @param l link.
	 */
	public void setlink(final Node l) {
		link = l;
	}
	/**
	 *
	 * @param e object.
	 */
	public void setdata(final Object e) {
		element = e;
	}
	/**
	 *
	 * @return data.
	 */
	Object getdata() {
		return element;
	}
	/**
	 *
	 * @return link.
	 */
	Node getlink() {
		return link;
	}
}

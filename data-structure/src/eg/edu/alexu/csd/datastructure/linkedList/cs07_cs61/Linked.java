package eg.edu.alexu.csd.datastructure.linkedList.cs07_cs61;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;
/**
 *
 * @author KCc
 *
 */
public class Linked implements ILinkedList {
	/**
	 * object.
	 */
	Object element1;
	/**
	 * size list.
	 */
	int size = 0;
	/**
	 * variable to index.
	 */
	int index1;
	/**
	 * head.
	 */
	Node start = null;
	/**
	 * tail.
	 */
	Node end = null;
	/**
	 * flag.
	 */
	private int flag = 0;
	@Override
	/**
	 * add element.
	 */
	public void add(final int index, final Object element) {
		// TODO Auto-generated method stub
		if (!element.equals(null)) {
        Node nptr = new Node(element, null);
        Node temp = start;
        size++;
        if (index == 0) {
        	if (start == null) {
                start = nptr;
                end = start;
                return;
            } else {
                nptr.setlink(start);
                start = nptr;
                return;
            }
        }
        final int in = 1;
        if (index == size - in) {
        	 if (start == null) {
                 start = nptr;
                 end = start;
                 return;
             } else {
                 end.setlink(nptr);
                 end = nptr;
                 return;
             }
        }
        for (int i = 0; i < size; i++) {
        	final int uu = 1;
        	if (i == index - uu) {
        		Node nptr1 = temp.getlink();
        		temp.setlink(nptr);
        		nptr.setlink(nptr1);
        		return;
        	}
        	temp = temp.getlink();
        }
        throw new RuntimeException();
		}
	}

	@Override
	/**
	 * add at last.
	 */
	public void add(final Object element) {
		// TODO Auto-generated method stub
		if (!element.equals(null)) {
		Node nptr = new Node(element, null);

		if (start == null) {
			start = nptr;
			end = nptr;
			size++;
			return;
		}
		end.setlink(nptr);
		end = nptr;
		size++;
	}
	}

	@Override
	/**
	 * get element with index.
	 */
	public Object get(final int index) {
		// TODO Auto-generated method stub
		if (start.getdata().equals(null)) {
			throw new RuntimeException();
		}
		if (index == 0) {
			return start.getdata();
		}

		Node temp = start;
		int i;
		for (i = 1; i < size && i <= index; i++) {
			temp = temp.getlink();
			if (i == index) {
				return temp.getdata();
			}
		}
		throw new RuntimeException();
	}

	@Override
	/**
	 * set data.
	 */
	public void set(final int index, final Object element) {
		// TODO Auto-generated method stub
        if (element.equals(null)) {
        	 throw new RuntimeException();
        }
		Node temp = start;
		for (int i = 0; i < size; i++) {
			if (i == index) {
				temp.setdata(element);
				return;
			}
		     	temp = temp.getlink();
		}
		throw new RuntimeException();
	}
	@Override
	/**
	 * clear list.
	 */
	public void clear() {
		// TODO Auto-generated method stub
		size = 0;
		flag = 1;
		start = null;
	}

	@Override
	/**
	 * check empty.
	 */
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (size == 0 || flag == 1 || start == null) {
			return true;
		}
		return false;
	}

	@Override
	/**
	 * remove data.
	 */
	public void remove(final int index) {
		// TODO Auto-generated method stub
		Node temp = start;
		Node temp1 = start;
		if (size == 1 && index == 0) {
			size--;
			start = null;
			return;
		}

		if (index == 0) {
			start = start.getlink();
			size--;
			return;
		}

		if (index == size - 1) {
			Node current = start;
	        while (current.getlink() != end) {
	            current = current.getlink();
	        }
	        current.setlink(null);
	        end = current;
	        size--;
	        return;
		}
		for (int i = 0; i < size; i++) {
			final int m = 1;
			if (i == index - m) {
				temp = temp.getlink();
				temp = temp.getlink();
				temp1.setlink(temp);
				size--;
				return;
			}
			temp = temp.getlink();
			temp1 = temp1.getlink();
		}
		throw new RuntimeException();
	}
    /**
     * sized.
     */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	/**
	 * give list.
	 */
	public ILinkedList sublist(final int fromIndex, final int toIndex) {
		// TODO Auto-generated method stub
		if (toIndex >= size || fromIndex < 0) {
			throw new RuntimeException();
		}
		Linked a = new Linked();
		Node temp = start;
		for (int i = 0; i < size; i++) {
			if (i == fromIndex) {
				while (i <= toIndex && i < size) {
					a.add(temp.getdata());
					temp = temp.getlink();
					i++;
				}
			}
			temp = temp.getlink();
		}
        if (a.size == 0) {
        	throw new RuntimeException();
        } else {
		return a;
        }
	}

	@Override
	/**
	 * check contain.
	 */
	public boolean contains(final Object o) {
		// TODO Auto-generated method stub
		if (o.equals(null)) {
			throw new RuntimeException();
		}
		Node temp = start;
		for (int i = 0; i < size; i++) {
			if (temp.getdata().equals(o)) {
				 return true;
			}
           temp = temp.getlink();
		}
		return false;
	}

}
